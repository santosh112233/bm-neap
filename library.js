
DOM.extend("[title]", {
    constructor: function() {
      var tooltip = DOM.create("span.custom-title");
  
      // set the title's textContent and hide it initially
      tooltip.set("textContent", this.get("title")).hide();
  
      this
        // remove legacy title
        .set("title", null)
        // store reference for quicker access
        .data("tooltip", tooltip)
        // register event handlers
        .on("mouseenter", this.onMouseEnter, ["clientX", "clientY"])
        .on("mouseleave", this.onMouseLeave)
        // insert the title element into DOM
        .append(tooltip);
    },
    // onMouseEnter: function(x, y) {
    //   this.data("tooltip").style({left: x, top: y}).show();
    // },
    // onMouseLeave: function() {
    //   this.data("tooltip").hide();
    // }
  });
  
